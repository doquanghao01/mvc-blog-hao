﻿CREATE TABLE [dbo].[Blog] (
    [Id]          INT            IDENTITY (1, 1) NOT NULL,
    [News]         NVARCHAR (MAX) NULL,
    [Type]        NVARCHAR (MAX) NULL,
    [Status]   INT            NULL,
    [Location]       NVARCHAR (MAX) NULL,
    [PublicDay] DATE           NULL,
    [Picture]     NVARCHAR (MAX) NULL,
    [Detail]     NVARCHAR (MAX) NULL,
    [Short description]    NVARCHAR (MAX) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);