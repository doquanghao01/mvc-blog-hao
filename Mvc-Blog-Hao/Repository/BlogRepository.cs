﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Web.Mvc;

namespace Mvc_Blog_Hao.Models
{
    public class BlogRepository
    {
        DbBlog dbblog = new DbBlog();
        //Get data from database
        public List<Blog> Get(string searchNews)
        {
            var blogs = dbblog.Blogs.ToList();
            if (!string.IsNullOrEmpty(searchNews))
            {
                blogs = dbblog.Blogs.Where(s => s.News.Contains(searchNews)).ToList();
            }
            return blogs;
        }

        //Get details from database
        public Blog GetDetail(int? id)
        {
            var blog = dbblog.Blogs.Find(id);
            return blog;
        }
        //Create to database
        public bool Create(Blog blog)
        {
            blog.Location = JsonConvert.SerializeObject(blog.Locations);
            dbblog.Blogs.Add(blog);
            dbblog.SaveChanges();
            if (GetDetail(blog.Id) == null)
            {
                return false;
            }
            return true;
        }
        //Delete to database
        public void Delete(int id)
        {
            Blog blog = dbblog.Blogs.Find(id);
            dbblog.Blogs.Remove(blog);
            dbblog.SaveChanges();
        }
        //Edit to database
        public bool Edit(Blog blog)
        {
            blog.Location = JsonConvert.SerializeObject(blog.Locations);
            dbblog.Blogs.AddOrUpdate(blog);
            dbblog.SaveChangesAsync();
            return true;
        }
    }
}