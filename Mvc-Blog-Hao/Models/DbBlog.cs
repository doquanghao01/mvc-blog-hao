using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Mvc_Blog_Hao.Models
{
    public partial class DbBlog : DbContext
    {
        public DbBlog()
            : base("name=ModelBlog")
        {
        }

        public virtual DbSet<Blog> Blogs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
