namespace Mvc_Blog_Hao.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("Blog")]
    public partial class Blog
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please add title!")]
        public string News { get; set; }
        public string Type { get; set; }

        public int? Status { get; set; }

        public string Location { get; set; }

        [Column(TypeName = "date")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Please select date!")]
        public DateTime? PublicDay { get; set; }


        [Required(ErrorMessage = "Please select the image!")]
        public string Picture { get; set; }


        [Required(ErrorMessage = "Please not to blank information!")]
        public string Detail { get; set; }


        [Required(ErrorMessage = "Please not to blank information!")]
        public string ShortDescription { get; set; }

        public List<string> Locations { get; set; }
        public SelectList ListStyle()
        {
            List<string> listStyle = new List<string>();
            listStyle.Add("Khinh Doanh");
            listStyle.Add("Gi?i tr�");
            listStyle.Add("Th? gi?i");
            listStyle.Add("Th?i s?");
            return new SelectList(listStyle, "", "");
        }
        public List<string> ListLocation()
        {
            List<string> listLocation = new List<string>();
            listLocation.Add("Vi?t Nam");
            listLocation.Add("Ch�u �");
            listLocation.Add("Ch�u �u");
            listLocation.Add("Ch�u M?");
            return listLocation;
        }

    }


}
