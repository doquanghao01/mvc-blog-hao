﻿using Mvc_Blog_Hao.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Mvc_Blog_Hao.Controllers
{
    public class HomeController : Controller
    {
        private BlogRepository blogRepository = new BlogRepository();
        private Blog mdblog=new Blog();

        // GET: blogs
        public ActionResult Index()
        {
            return View(blogRepository.Get(""));
        }
        public ActionResult Search(string searchNews)
        {
            return View(blogRepository.Get(searchNews));
        }
        public ActionResult Create()
        {
            ViewBag.ListStyle = mdblog.ListStyle();
            ViewBag.ListLocation = mdblog.ListLocation();
            return View();
        }
        //POST: blogs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Blog blog)
        {
            ViewBag.ListStyle = mdblog.ListStyle();
            ViewBag.ListLocation = mdblog.ListLocation();
            if (blogRepository.Create(blog))
            {
                return RedirectToAction("Index");
            }
            return View(blog);
        }

        // GET: blogs/Edit/5
        public ActionResult Edit(int? id)
        {
            ViewBag.ListStyle = mdblog.ListStyle();
            ViewBag.ListLocation = mdblog.ListLocation();
            ViewBag.Models = blogRepository.GetDetail(id).Location;
            if (blogRepository.GetDetail(id) == null)
            {
                return HttpNotFound();
            }
            return View(blogRepository.GetDetail(id));
        }

        // POST: blogs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Blog blog)
        {
            ViewBag.ListStyle = mdblog.ListStyle();
            ViewBag.ListLocation = mdblog.ListLocation();
            ViewBag.Models = blogRepository.GetDetail(blog.Id).Location;

            if (ModelState.IsValid)
            {
                if(blogRepository.Edit(blog))
                {
                    return RedirectToAction("Index");

                }
            }
            return View(blog);
        }

        // Delete: Blogs/Delete/5

        [HttpPost]
        public JsonResult DeleteConfirmed(int id)
        {

            blogRepository.Delete(id);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

    }
}